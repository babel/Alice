define(function (require, exports, module) {
    var PreferencesManager  = brackets.getModule("preferences/PreferencesManager"),
        StateManager        = PreferencesManager.stateManager,
        prefix              = "nullpo.alice-issuetracker";

    var defaultPreferences = [
        {
            key: "githubAccessToken",
            type: "string",
            value:""
        }
    ]

    var byKey = function(key){
        return function(elem){
            return elem? elem.key : false;
        }
    }

    var prefs = PreferencesManager.getExtensionPrefs(prefix);

    // Add default preferences)
    defaultPreferences.forEach(function (definition) {
        if (definition.os && definition.os[brackets.platform]) {
            prefs.definePreference(definition.key, definition.type,
                                   definition.os[brackets.platform].value);
        } else {
            prefs.definePreference(definition.key, definition.type, definition.value);
        }
        return true;
    });

    prefs.save();

    function get(key) {
        var location = defaultPreferences.filter(byKey(key)).length ? PreferencesManager : StateManager;
        arguments[0] = prefix + "." + key;
        return location.get.apply(location, arguments);
    }

    function set(key) {
        var location = defaultPreferences.filter(byKey(key)).length ? PreferencesManager : StateManager;
        arguments[0] = prefix + "." + key;
        return location.set.apply(location, arguments);
    }

    function getAll() {
        var obj = {};
        defaultPreferences.forEach(function (definition) {
            obj[definition.key] = get(definition.key);
        });
        return obj;
    }

    function getDefaults() {
        var obj = {};
        defaultPreferences.forEach(function (definition) {
            var defaultValue;
            if (definition.os && definition.os[brackets.platform]) {
                defaultValue = definition.os[brackets.platform].value;
            } else {
                defaultValue = definition.value;
            }
            obj[definition.key] = defaultValue;
        });
        return obj;
    }

    function getGlobal(key) {
        return PreferencesManager.get(key);
    }

    function persist(key, value) {
        // FUTURE: remote this method
        set(key, value);
        save();
    }

    function save() {
        PreferencesManager.save();
        StateManager.save();
    }

    module.exports = {
        get: get,
        set: set,
        getAll: getAll,
        getDefaults: getDefaults,
        getGlobal: getGlobal,
        persist: persist,
        save: save
    };


})
