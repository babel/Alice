/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, $, brackets */

define(function (require, exports, module) {
    exports.i18n = {
        MNU_TRACKING         : "Tracking",
        MNU_GITHUB           : "Github issues...",
        BTN_TOGGLE           : "Show Alice Tracker panel",
        BTN_ISSUES_OPEN      : "Open issues",
        BTN_ISSUES_CLOSE     : "Closed issues",
        BTN_ISSUES_ALL       : "All issues",
        BTN_ISSUES_BUGS      : "Open bugs",
        LBL_LOADING          : "Loading...",
        BTN_ISSUE_DETAIL     : "Detalle"
    }
})
